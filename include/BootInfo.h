//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**          BootInfo.h                                                  **
**                                                                      **
**          Copyright (C), 2015-2015, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

#if !defined( MYOS1_KERNEL_INCLUDED_BOOT_INFO_H )
#    define   MYOS1_KERNEL_INCLUDED_BOOT_INFO_H

#define     MULTIBOOT_HEADER_MAGIC      0x1BADB002
#define     MULTIBOOT_HEADER_FLAGS      0x00000000

#define     MULTIBOOT_LOADER_MAGIC      0x2BADB002

#if defined(  MYOS1_INCLUDE_FROM_C )

#endif  //  if defined(MYOS1_INCLUDE_FROM_C)    //

#endif
